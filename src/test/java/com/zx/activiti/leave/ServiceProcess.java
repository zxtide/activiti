package com.zx.activiti.leave;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceProcess {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    // 查询用户任务
    @Test
    public void getUserTasks() {

        String assignee = "U02";

        taskService.createTaskQuery().taskAssignee(assignee).list().forEach(data -> System.out.println(data.toString()));
    }

    // 查询管理员任务
    @Test
    public void getManagerTasks() {

        String assignee = "PM02";

        taskService.createTaskQuery().taskAssignee(assignee).list().forEach(data -> System.out.println(data.toString()));
    }

    // 管理员审批成功
    @Test
    public void approver_ok() throws InterruptedException {

        String taskId = "17509";

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pass", true);

        // 审批流程
        taskService.complete(taskId, map);
    }

    // 管理员驳回
    @Test
    public void approver_ng() {

        String taskId = "42505";

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pass", false);

        // 审批流程
        taskService.complete(taskId, map);
    }


}
