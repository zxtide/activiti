package com.zx.activiti.leave;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StartProcess {


	@Autowired
	RuntimeService runtimeService;

	@Autowired
    TaskService taskService;
	
	@Autowired
	ProcessEngine processEngine;
	
	@Autowired
	HistoryService historyService;
	
	@Autowired
	RepositoryService repositoryService;


    @Test
    public void startProcess() {

        // 用户提交申请，创建流程实例
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("user_id", "U02");
        map.put("approver_id", "PM02");
        
        ExecutionEntity pi = (ExecutionEntity)runtimeService.startProcessInstanceByKey("leave_process", map);
        String taskId = pi.getTasks().get(0).getId();
        taskService.complete(taskId);
        
        Task nextTask = taskService.createTaskQuery().processInstanceId(pi.getId()).singleResult();
        System.out.println("当前活动taskid:"+nextTask.getId());
    }
    
    // 所有步骤
    @Test
    public void startProcess2() {
    	
    	// 1.用户触发流程事件(点击提交申请按钮等)
    	
    	// 2.启动流程
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("user_id", "U02");
    	map.put("approver_id", "PM02");
    	ExecutionEntity pi = (ExecutionEntity)runtimeService.startProcessInstanceByKey("leave_process", map);
    	
        String process_id = pi.getId();
        String taskId1 = pi.getTasks().get(0).getId();
        System.out.println("--提交申请-taskid : "+taskId1);
        
    	// 3.完成第一步提交申请
    	taskService.complete(taskId1, map);
    	
    	// 4.查询当前流程节点taskid
    	Task task2 = taskService.createTaskQuery().processInstanceId(process_id).singleResult();
    	System.out.println("--审批-taskid : "+task2.getId());
    	
    	// 5.管理员审批成功
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("pass", true);
        taskService.complete(task2.getId(), map2);
        
        // 6.自动执行服务发送消息
    }
    
    // 查询流程定义
    @Test
    public void getDefinition() {
    	
    	repositoryService.createProcessDefinitionQuery()
    	.orderByProcessDefinitionVersion()
    	.asc()
    	.list()
    	.forEach(data->System.out.println(data.getId()));
    }
    
    /**查询历史任务*/  
    @Test  
    public void findHistoryTask(){  
        String taskAssignee = "U02";  
        List<HistoricTaskInstance> list = historyService//与历史数据（历史表）相关的Service  
                        .createHistoricTaskInstanceQuery()//创建历史任务实例查询  
                        .taskAssignee(taskAssignee)//指定历史任务的办理人  
                        .list();  
        if(list!=null && list.size()>0){  
            for(HistoricTaskInstance hti:list){  
                System.out.println(hti.getId()+"    "+hti.getName()+"    "+hti.getProcessInstanceId()+"   "+hti.getStartTime()+"   "+hti.getEndTime()+"   "+hti.getDurationInMillis());  
                System.out.println("################################");  
            }  
        }  
    } 
    
    /**查询历史流程实例*/  
    @Test  
    public void findHistoryProcessInstance(){  
        String processInstanceId = "15001";  
        HistoricProcessInstance hpi = historyService//与历史数据（历史表）相关的Service  
                        .createHistoricProcessInstanceQuery()//创建历史流程实例查询  
                        .processInstanceId(processInstanceId)//使用流程实例ID查询  
                        .singleResult();  
        System.out.println(hpi.getId()+"    "+hpi.getProcessDefinitionId()+"    "+hpi.getStartTime()+"    "+hpi.getEndTime()+"     "+hpi.getDurationInMillis());  
    }  
}
