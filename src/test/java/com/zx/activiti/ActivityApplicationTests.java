package com.zx.activiti;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivityApplicationTests {


	@Autowired
	RuntimeService runtimeService;

	@Autowired
    TaskService taskService;

    @Test
    public void TestServiceTask() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("applicantName", "John Doe");
        variables.put("email", "john.doe@activiti.com");
        variables.put("phoneNumber", "123456789");
        runtimeService.startProcessInstanceByKey("myProcess_1", variables);
    }

    // -- 运行过程
    // -- 1.执行TestUserTask,模拟一个流程的执行
    // -- 2.指定某个人查询

    // 运行task流
    @Test
    public void TestUserTask() {
        System.out.println("------begin------");

        // 全局变量,启动时加载
        Map<String, Object> map = new HashMap<>();
        map.put("apply", "zhangsan");
        map.put("approve", "lisi");

        // 流程启动
        ExecutionEntity pi = (ExecutionEntity)runtimeService.startProcessInstanceByKey("myProcess_2", map);
        String process_id = pi.getId();

        String taskId1 = pi.getTasks().get(0).getId();
        taskService.complete(taskId1, map);// 完成第一步申请

        Task task = taskService.createTaskQuery().processInstanceId(process_id).singleResult();
        String taskId2 = task.getId();

        // 局部变量 - 从该时间点产生
        map.put("pass", true);

        taskService.complete(taskId2, map); // 驳回申请

        System.out.println("------end------");
    }

    // 查询指定用户的任务
    @Test
    public void TestGetTask() {

        List<Task> list = taskService.createTaskQuery().taskAssignee("zhangsan").list();
        list.forEach(task -> System.out.println(task.toString()));
    }
}
